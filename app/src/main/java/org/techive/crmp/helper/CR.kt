package org.techive.crmp.helper

import android.support.multidex.MultiDexApplication
import com.google.android.gms.ads.MobileAds
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import org.techive.crmp.models.User

class CR : MultiDexApplication() {
    companion object {
        val ALLCRIMERS = "all-crimes"
        val ALLMISSINGS = "all-missing-cases"
        val ALLCOMPLAINTS = "all-complaints"
        val FRAGSTATUS = "fragment-status"
        val DATASTATUS = "data-status"
        val DETAILSTATUS = "detail-status"
        val ACTSTATUS = "activity-status"
        var u: User? = null

        fun getAuth(): FirebaseAuth {
            return FirebaseAuth.getInstance()
        }

        fun getUserId(): String {
            return FirebaseAuth.getInstance().currentUser!!.uid
        }

        fun isSignedIn(): Boolean {
            return FirebaseAuth.getInstance().currentUser != null
        }

        fun getDatabaseRef(): DatabaseReference {
            return FirebaseDatabase.getInstance().reference
        }

        fun getStorageRef(): StorageReference {
            return FirebaseStorage.getInstance().reference
        }

        fun getUploadTask(task: UploadTask): UploadTask {
            return task
        }
    }
}
