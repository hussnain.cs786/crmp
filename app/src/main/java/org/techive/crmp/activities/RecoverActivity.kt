package org.techive.crmp.activities

import android.app.ProgressDialog
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.WindowManager
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_recover.*
import org.techive.crmp.R
import org.techive.crmp.helper.CR
import org.techive.crmp.helper.InputValidation

class RecoverActivity : AppCompatActivity() {

    var inputValidation: InputValidation? = null
    private var mProgress: ProgressDialog? = null

    internal var e: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recover)
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)
        inputValidation = InputValidation(this)
        mProgress = ProgressDialog(this)

        title = "Recover"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        recover.setOnClickListener {
            e = email.text.toString()

            if (!inputValidation!!.isInputEditTextFilled(email, getString(R.string.error_message_email))) {
                return@setOnClickListener
            }
            if (!inputValidation!!.isInputEditTextEmail(email, getString(R.string.error_message_email))) {
                return@setOnClickListener
            }else{
                mProgress!!.setTitle(getString(R.string.password_send))
                mProgress!!.setMessage(getString(R.string.signing_up_message))
                mProgress!!.setCanceledOnTouchOutside(false)
                mProgress!!.show()
                CR.getAuth().sendPasswordResetEmail(e)
                    .addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                            mProgress!!.dismiss()
                            Toast.makeText(this, R.string.check_your_email, Toast.LENGTH_LONG).show()
                            onBackPressed()
                        } else {
                            mProgress!!.hide()
                            Toast.makeText(this, R.string.error_message_email, Toast.LENGTH_LONG).show()
                        }
                    }
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            android.R.id.home -> {

                onBackPressed()

            }
        }
        return super.onOptionsItemSelected(item)
    }
}
