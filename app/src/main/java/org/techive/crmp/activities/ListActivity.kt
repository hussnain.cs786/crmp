package org.techive.crmp.activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.SearchView
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.activity_list.*
import org.jetbrains.anko.toast
import org.techive.crmp.R
import org.techive.crmp.adapters.RecyclerViewAdapter
import org.techive.crmp.helper.CR
import org.techive.crmp.models.Complaints
import org.techive.crmp.models.Crimes
import org.techive.crmp.models.Missings

class ListActivity : AppCompatActivity() {

    var mList: MutableList<Any> = ArrayList()
    var mAdapter: RecyclerViewAdapter? = null
    private var mListType = RecyclerViewAdapter.VIEW_TYPE_CRIME
    var ref = CR.getDatabaseRef().child(CR.ALLCRIMERS)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list)
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)
        progressbar.visibility = android.view.View.GONE
        search_view.clearFocus()

        //status = 0 -> crime     status = 1 -> missing person      status = 2 -> complaints
        val status = intent.getStringExtra(CR.ACTSTATUS)

        when (status) {
            "0" -> {
                //crimes
                search_view.queryHint = "Enter street, city or zipcode"
                mListType = RecyclerViewAdapter.VIEW_TYPE_CRIME
                ref = CR.getDatabaseRef().child(CR.ALLCRIMERS)
            }
            "1" -> {
                //missing persons
                search_view.queryHint = "Enter name to find missing person"
                mListType = RecyclerViewAdapter.VIEW_TYPE_MISSING
                ref = CR.getDatabaseRef().child(CR.ALLMISSINGS)
            }
            "2" -> {
                //complaints
                search_view.queryHint = "Enter city, address or pincode"
                mListType = RecyclerViewAdapter.VIEW_TYPE_COMPLAINTS
                ref = CR.getDatabaseRef().child(CR.ALLCOMPLAINTS)
            }
        }

        search_view.setOnQueryTextListener(object : SearchView.OnQueryTextListener,
            android.support.v7.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(text: String): Boolean {
                loadData(mListType, ref, text, status)
                search_view.clearFocus()
                progressbar.visibility = android.view.View.VISIBLE
                return true
            }

            override fun onQueryTextChange(text: String?): Boolean {
                return true
            }
        })
    }

    fun loadData(
        viewtype: Int,
        ref: DatabaseReference,
        text: String,
        status: String
    ) {
        ref.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError?) {

            }

            override fun onDataChange(dataSnapshot: DataSnapshot?) {
                Log.e("LIST:", "OK")
                mList.clear()
                for (dataSnapshot1 in dataSnapshot!!.children) {

                    when (status) {
                        "0" -> {
                            //crime
                            val model = dataSnapshot1.getValue(Crimes::class.java)!!
                            if (model.city!!.toLowerCase().contains(text.toLowerCase())
                                || model.zipcode!!.toLowerCase().contains(text.toLowerCase())
                                || model.street!!.toLowerCase().contains(text.toLowerCase())
                                || model.details!!.toLowerCase().contains(text.toLowerCase())
                            )
                                mList.add(0, model)
                        }
                        "1" -> {
                            //missing person
                            val model = dataSnapshot1.getValue(Missings::class.java)!!
                            if (model.name!!.toLowerCase().contains(text.toLowerCase())
                                || model.age!!.toLowerCase().contains(text.toLowerCase())
                                || model.lastseen!!.toLowerCase().contains(text.toLowerCase())
                                || model.details!!.toLowerCase().contains(text.toLowerCase())
                            )
                                mList.add(0, model)
                        }
                        "2" -> {
                            //complaints
                            val model = dataSnapshot1.getValue(Complaints::class.java)!!
                            if (model.city!!.toLowerCase().contains(text.toLowerCase())
                                || model.address!!.toLowerCase().contains(text.toLowerCase())
                                || model.pincode!!.toLowerCase().contains(text.toLowerCase())
                                || model.subject!!.toLowerCase().contains(text.toLowerCase())
                                || model.complaint!!.toLowerCase().contains(text.toLowerCase())
                            )
                                mList.add(0, model)
                        }
                    }
                    progressbar.visibility = View.GONE
                }
                mAdapter = RecyclerViewAdapter(mList, viewtype)
                with(recycler_view) {
                    layoutManager = android.support.v7.widget.LinearLayoutManager(context)

                    adapter = mAdapter
                    setEmptyView(empty_list_view)
                    progressbar.visibility = android.view.View.GONE
                }
            }

        })
    }

    override fun onBackPressed() {
        if (search_view.query != "") {
            search_view.clearFocus()
            search_view.setQuery("", false)
        }
        super.onBackPressed()
    }
}
