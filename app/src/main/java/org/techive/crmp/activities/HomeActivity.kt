package org.techive.crmp.activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_home.*
import org.techive.crmp.R
import org.techive.crmp.helper.CR

class HomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        crimes.setOnClickListener {
            val intent = Intent(this@HomeActivity, ListActivity::class.java)
            intent.putExtra(CR.ACTSTATUS,"0")
            startActivity(intent)
        }
        missings.setOnClickListener {
            val intent = Intent(this@HomeActivity, ListActivity::class.java)
            intent.putExtra(CR.ACTSTATUS,"1")
            startActivity(intent)
        }
        complaints.setOnClickListener {
            val intent = Intent(this@HomeActivity, ListActivity::class.java)
            intent.putExtra(CR.ACTSTATUS,"2")
            startActivity(intent)
        }
        login.setOnClickListener {
            startActivity(Intent(this@HomeActivity, LoginActivity::class.java))
        }
    }
}
