package org.techive.crmp.activities

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.MenuItem
import android.view.WindowManager
import android.widget.Toast
import com.google.android.gms.ads.InterstitialAd
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.activity_login.*
import org.techive.crmp.R
import org.techive.crmp.helper.CR
import org.techive.crmp.helper.InputValidation
import org.techive.crmp.models.User

class LoginActivity : AppCompatActivity() {

    var inputValidation: InputValidation? = null
    private var mProgress: ProgressDialog? = null

    internal var e: String = ""
    internal var p: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)
        inputValidation = InputValidation(this)
        mProgress = ProgressDialog(this)

        title = "Login"

        login.setOnClickListener {
            e = email.text.toString()
            p = password.text.toString()

            if (!inputValidation!!.isInputEditTextFilled(email, getString(R.string.error_message_email))) {
                return@setOnClickListener
            }
            if (!inputValidation!!.isInputEditTextEmail(email, getString(R.string.error_message_email))) {
                return@setOnClickListener
            }
            if (!inputValidation!!.isInputEditTextFilled(password, getString(R.string.error_message_password))) {
                return@setOnClickListener
            } else {
                mProgress!!.setTitle("Logging In")
                mProgress!!.setMessage("Please wait while we check your credentials.")
                mProgress!!.setCanceledOnTouchOutside(false)
                mProgress!!.show()

                CR.getAuth().signInWithEmailAndPassword(e, p)
                    .addOnCompleteListener(this, object : OnCompleteListener<AuthResult> {
                        override fun onComplete(task: Task<AuthResult>) {
                            if (task.isSuccessful()) {
                                CR.getDatabaseRef()
                                    .child("users")
                                    .child(CR.getUserId())
                                    .addListenerForSingleValueEvent(object : ValueEventListener {
                                        override fun onDataChange(dataSnapshot: DataSnapshot) {
                                            Log.e("OK", "OK")
                                            val u = dataSnapshot.getValue(User::class.java)
                                            if (u != null) {
                                                CR.u = u
                                                mProgress!!.dismiss()
                                                val intent =
                                                    Intent(this@LoginActivity, MainActivity::class.java)
                                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                                                startActivity(intent)
                                            } else {
                                                Toast.makeText(
                                                    this@LoginActivity,
                                                    R.string.something_went_wrong,
                                                    Toast.LENGTH_LONG
                                                ).show()
                                            }
                                        }

                                        override fun onCancelled(databaseError: DatabaseError) {
                                            mProgress!!.dismiss()
                                            Log.e("Error", databaseError.toException().message)
                                        }
                                    })

                                Log.d("Login", "signInWithEmail:success")

                            } else {
                                // If sign in fails, display a message to the user.
                                Log.w("Login", "signInWithEmail:failure", task.exception)
                                mProgress!!.dismiss()
                                Toast.makeText(
                                    this@LoginActivity,
                                    "Error : " + task.exception!!.message,
                                    Toast.LENGTH_LONG
                                ).show()
                            }
                        }
                    })
            }

        }

        create_account.setOnClickListener {
            startActivity(Intent(this@LoginActivity, RegisterActivity::class.java))
        }

        forgot_password.setOnClickListener {
            startActivity(Intent(this@LoginActivity, RecoverActivity::class.java))
        }
    }
    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            android.R.id.home -> onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }
}
