package org.techive.crmp.activities

import android.Manifest
import android.app.Activity
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.text.InputType
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import com.google.android.gms.tasks.Continuation
import com.google.android.gms.tasks.Task
import com.google.firebase.database.DatabaseReference
import com.google.firebase.storage.UploadTask
import com.nabinbhandari.android.permissions.PermissionHandler
import com.nabinbhandari.android.permissions.Permissions
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_add.*
import org.jetbrains.anko.toast
import org.techive.crmp.R
import org.techive.crmp.helper.CR
import org.techive.crmp.helper.InputValidation
import org.techive.crmp.models.Complaints
import org.techive.crmp.models.Crimes
import org.techive.crmp.models.Missings
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

class AddActivity : AppCompatActivity() {

    var s1 = ""
    var s2 = ""
    var s3 = ""
    var s4 = ""
    var s5 = ""
    var gender = "male"

    private val GALLERY = 100
    private val CAMERA = 10

    var filepath: Uri? = null

    val permissions = arrayOf(
        Manifest.permission.CAMERA,
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    )
    val rationale = "Please provide permissions so that you can ..."
    val options = Permissions.Options()
        .setRationaleDialogTitle("Info")
        .setSettingsDialogTitle("Warning")

    var inputValidation: InputValidation? = null
    private var mProgress: ProgressDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add)
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        mProgress = ProgressDialog(this)
        inputValidation = InputValidation(this)

        //crime: street, city, zipcode, details, image
        //complaints: address, city, pincode, subject, complaints, photo
        //missing: name, age, gender, last seen, details, image

        // 0: crime     1: complaint        2:missing
        val fragStatus = intent.getStringExtra(CR.FRAGSTATUS)
        val status = intent.getStringExtra(CR.DATASTATUS)
        when (fragStatus) {
            "0" -> {
                //crime
                tvone.text = "Street *"
                etone.hint = "Enter street"
                etone.inputType = InputType.TYPE_CLASS_TEXT

                tvtwo.text = "City *"
                ettwo.hint = "Enter city"
                ettwo.inputType = InputType.TYPE_CLASS_TEXT

                tvthree.text = "Zipcode *"
                etthree.hint = "Enter zipcode"
                etthree.inputType = InputType.TYPE_CLASS_NUMBER

                tvfive.text = "Crime details *"
                etfive.hint = "Enter crime details..."

                tvfour.visibility = View.GONE
                etfour.visibility = View.GONE

                tvgender.visibility = View.GONE
                segmentedbutton.visibility = View.GONE

                if (status == "add") {
                    title = "Add crime report"

                    submit.setOnClickListener {
                        s1 = etone.text.toString()      //street
                        s2 = ettwo.text.toString()      //city
                        s3 = etthree.text.toString()    //zipcode
                        s4 = etfive.text.toString()     //details

                        if (filepath == null) {
                            toast("Please pick an image")
                            return@setOnClickListener
                        }
                        if (!inputValidation!!.isInputEditTextFilled(etone, "Enter street")) {
                            return@setOnClickListener
                        }
                        if (!inputValidation!!.isInputEditTextFilled(ettwo, "Enter city")) {
                            return@setOnClickListener
                        }
                        if (!inputValidation!!.isInputEditTextFilled(etthree, "Enter zipcode")) {
                            return@setOnClickListener
                        }
                        if (!inputValidation!!.isInputEditTextFilled(etfive, "Enter details")) {
                            return@setOnClickListener
                        } else {
                            uploadData("0", s1, s2, s3, s4, "")
                        }
                    }
                } else
                    if (status == "update") {
                        title = "Update crime report"
                        val model = intent.getSerializableExtra("MODEL") as Crimes
                        etone.setText(model.street)
                        ettwo.setText(model.city)
                        etthree.setText(model.zipcode)
                        etfive.setText(model.details)
                        Picasso.get().load(model.thumbnail).placeholder(R.drawable.ic_add_a_photo).into(display_image)

                        submit.setOnClickListener {
                            s1 = etone.text.toString()      //street
                            s2 = ettwo.text.toString()      //city
                            s3 = etthree.text.toString()    //zipcode
                            s4 = etfive.text.toString()     //details

                            if (!inputValidation!!.isInputEditTextFilled(etone, "Enter street")) {
                                return@setOnClickListener
                            }
                            if (!inputValidation!!.isInputEditTextFilled(ettwo, "Enter city")) {
                                return@setOnClickListener
                            }
                            if (!inputValidation!!.isInputEditTextFilled(etthree, "Enter zipcode")) {
                                return@setOnClickListener
                            }
                            if (!inputValidation!!.isInputEditTextFilled(etfive, "Enter details")) {
                                return@setOnClickListener
                            } else {
                                mProgress!!.setTitle("Updating...")
                                mProgress!!.setMessage("Please wait while we check your credentials.")
                                mProgress!!.setCanceledOnTouchOutside(false)
                                mProgress!!.show()
                                val ref = CR.getStorageRef().child("images").child(model.key.toString())
                                val time =
                                    SimpleDateFormat("EEE, d MMM yyyy")
                                        .format(Date(System.currentTimeMillis()))
                                var m: Any? = null
                                if (filepath != null) {
                                    val uploadTask = ref.putFile(filepath!!)
                                    val urlTask =
                                        uploadTask.continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>> { task ->
                                            if (!task.isSuccessful) {
                                                task.exception?.let {
                                                    throw it
                                                }
                                            }
                                            return@Continuation ref.downloadUrl
                                        }).addOnCompleteListener { task ->
                                            if (task.isSuccessful) {
                                                val downloadUri = task.result

                                                Log.e("IMAGE URL: ", downloadUri.toString())

                                                m = Crimes(
                                                    model.key,
                                                    CR.getUserId(),
                                                    s1,
                                                    s2,
                                                    s3,
                                                    s4,
                                                    downloadUri.toString(),
                                                    model.status,
                                                    time
                                                )
                                                CR.getDatabaseRef().child(CR.ALLCRIMERS)!!
                                                    .child(model.key)
                                                    .setValue(m!!)
                                                    .addOnCompleteListener { task ->
                                                        if (task.isSuccessful) {
                                                            mProgress!!.dismiss()
                                                            finish()
                                                            toast("Data updated successfully!")
                                                        } else {
                                                            toast("Something went wrong")
                                                            mProgress!!.dismiss()
                                                        }
                                                    }
                                            } else {
                                                // Handle failures
                                                toast("Something went wrong!")
                                                mProgress!!.dismiss()
                                            }
                                        }
                                } else {
                                    m = Crimes(
                                        model.key,
                                        CR.getUserId(),
                                        s1,
                                        s2,
                                        s3,
                                        s4,
                                        model.thumbnail,
                                        model.status,
                                        time
                                    )
                                    CR.getDatabaseRef().child(CR.ALLCRIMERS)!!
                                        .child(model.key)
                                        .setValue(m!!)
                                        .addOnCompleteListener { task ->
                                            if (task.isSuccessful) {
                                                mProgress!!.dismiss()
                                                finish()
                                                toast("Data updated successfully!")
                                            } else {
                                                toast("Something went wrong")
                                                mProgress!!.dismiss()
                                            }
                                        }
                                }

                            }
                        }
                    }

            }
            "1" -> {
                //complaint
                tvone.text = "Address *"
                etone.hint = "Enter address"
                etone.inputType = InputType.TYPE_CLASS_TEXT

                tvtwo.text = "City *"
                ettwo.hint = "Enter city"
                ettwo.inputType = InputType.TYPE_CLASS_TEXT

                tvthree.text = "Pincode *"
                etthree.hint = "Enter pincode"
                etthree.inputType = InputType.TYPE_CLASS_NUMBER

                tvfour.text = "Subject *"
                etfour.hint = "Enter subject"
                etfour.inputType = InputType.TYPE_CLASS_TEXT

                tvfive.text = "Complaint details *"
                etfive.hint = "Enter complaint details..."

                tvgender.visibility = View.GONE
                segmentedbutton.visibility = View.GONE

                if (status == "add") {
                    title = "Add complaint"

                    submit.setOnClickListener {
                        s1 = etone.text.toString()      //address
                        s2 = ettwo.text.toString()      //city
                        s3 = etthree.text.toString()    //pincode
                        s4 = etfour.text.toString()     //subject
                        s5 = etfive.text.toString()     //details
                        if (filepath == null) {
                            toast("Please pick an image")
                            return@setOnClickListener
                        }
                        if (!inputValidation!!.isInputEditTextFilled(etone, "Enter address")) {
                            return@setOnClickListener
                        }
                        if (!inputValidation!!.isInputEditTextFilled(ettwo, "Enter city")) {
                            return@setOnClickListener
                        }
                        if (!inputValidation!!.isInputEditTextFilled(etthree, "Enter pincode")) {
                            return@setOnClickListener
                        }
                        if (!inputValidation!!.isInputEditTextFilled(etfive, "Enter subject")) {
                            return@setOnClickListener
                        }
                        if (!inputValidation!!.isInputEditTextFilled(etfive, "Enter details")) {
                            return@setOnClickListener
                        } else {
                            uploadData("1", s1, s2, s3, s4, s5)
                        }

                    }
                } else if (status == "update") {
                    title = "Update complaint"
                    val model = intent.getSerializableExtra("MODEL") as Complaints
                    etone.setText(model.address)
                    ettwo.setText(model.city)
                    etthree.setText(model.pincode)
                    etfour.setText(model.subject)
                    etfive.setText(model.complaint)
                    Picasso.get().load(model.thumbnail).placeholder(R.drawable.ic_add_a_photo).into(display_image)

                    submit.setOnClickListener {
                        s1 = etone.text.toString()      //address
                        s2 = ettwo.text.toString()      //city
                        s3 = etthree.text.toString()    //pincode
                        s4 = etfour.text.toString()     //subject
                        s5 = etfive.text.toString()     //details

                        if (!inputValidation!!.isInputEditTextFilled(etone, "Enter address")) {
                            return@setOnClickListener
                        }
                        if (!inputValidation!!.isInputEditTextFilled(ettwo, "Enter city")) {
                            return@setOnClickListener
                        }
                        if (!inputValidation!!.isInputEditTextFilled(etthree, "Enter pincode")) {
                            return@setOnClickListener
                        }
                        if (!inputValidation!!.isInputEditTextFilled(etfive, "Enter subject")) {
                            return@setOnClickListener
                        }
                        if (!inputValidation!!.isInputEditTextFilled(etfive, "Enter details")) {
                            return@setOnClickListener
                        } else {
                            mProgress!!.setTitle("Updating...")
                            mProgress!!.setMessage("Please wait while we check your credentials.")
                            mProgress!!.setCanceledOnTouchOutside(false)
                            mProgress!!.show()
                            val ref = CR.getStorageRef().child("images").child(model.key.toString())
                            val time =
                                SimpleDateFormat("EEE, d MMM yyyy")
                                    .format(Date(System.currentTimeMillis()))
                            var m: Any? = null
                            if (filepath != null) {
                                val uploadTask = ref.putFile(filepath!!)
                                val urlTask =
                                    uploadTask.continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>> { task ->
                                        if (!task.isSuccessful) {
                                            task.exception?.let {
                                                throw it
                                            }
                                        }
                                        return@Continuation ref.downloadUrl
                                    }).addOnCompleteListener { task ->
                                        if (task.isSuccessful) {
                                            val downloadUri = task.result

                                            Log.e("IMAGE URL: ", downloadUri.toString())


                                            m = Complaints(
                                                model.key,
                                                CR.getUserId(),
                                                s1,
                                                s2,
                                                s3,
                                                s4,
                                                s5,
                                                downloadUri.toString(),
                                                model.status,
                                                time
                                            )
                                            CR.getDatabaseRef().child(CR.ALLCOMPLAINTS)!!
                                                .child(model.key)
                                                .setValue(m!!)
                                                .addOnCompleteListener { task ->
                                                    if (task.isSuccessful) {
                                                        mProgress!!.dismiss()
                                                        finish()
                                                        toast("Data updated successfully!")
                                                    } else {
                                                        toast("Something went wrong")
                                                        mProgress!!.dismiss()
                                                    }
                                                }
                                        } else {
                                            // Handle failures
                                            toast("Something went wrong!")
                                            mProgress!!.dismiss()
                                        }
                                    }
                            } else {
                                m = Complaints(
                                    model.key,
                                    CR.getUserId(),
                                    s1,
                                    s2,
                                    s3,
                                    s4,
                                    s5,
                                    model.thumbnail,
                                    model.status,
                                    time
                                )
                                CR.getDatabaseRef().child(CR.ALLCOMPLAINTS)!!
                                    .child(model.key)
                                    .setValue(m!!)
                                    .addOnCompleteListener { task ->
                                        if (task.isSuccessful) {
                                            mProgress!!.dismiss()
                                            finish()
                                            toast("Data updated successfully!")
                                        } else {
                                            toast("Something went wrong")
                                            mProgress!!.dismiss()
                                        }
                                    }
                            }
                        }

                    }
                }
            }
            "2" -> {
                //missing
                tvone.text = "Name *"
                etone.hint = "Enter name"
                etone.inputType = InputType.TYPE_CLASS_TEXT

                tvtwo.text = "Age *"
                ettwo.hint = "Enter age"
                ettwo.inputType = InputType.TYPE_CLASS_NUMBER

                tvthree.text = "Last seen *"
                etthree.hint = "Enter last seen"
                etthree.inputType = InputType.TYPE_CLASS_TEXT

                tvfive.text = "Missing person details *"
                etfive.hint = "Enter missing person details..."

                tvfour.visibility = View.GONE
                etfour.visibility = View.GONE

                if (status == "add") {
                    title = "Add missing person"

                    segmentedbutton.setOnClickedButtonPosition { position ->
                        when (position.toString()) {
                            "0" -> {
                                gender = "Male"
                                toast(gender)
                            }
                            "1" -> {
                                gender = "Female"
                                toast(gender)
                            }
                            "2" -> {
                                gender = "Other"
                                toast(gender)
                            }
                        }
                    }

                    submit.setOnClickListener {
                        s1 = etone.text.toString()      //name
                        s2 = ettwo.text.toString()      //age
                        s3 = etthree.text.toString()    //last seen
                        s4 = etfive.text.toString()     //details

                        if (filepath == null) {
                            toast("Please pick an image")
                            return@setOnClickListener
                        }
                        if (!inputValidation!!.isInputEditTextFilled(etone, "Enter name")) {
                            return@setOnClickListener
                        }
                        if (!inputValidation!!.isInputEditTextFilled(ettwo, "Enter age")) {
                            return@setOnClickListener
                        }
                        if (!inputValidation!!.isInputEditTextFilled(etthree, "Enter last seen")) {
                            return@setOnClickListener
                        }
                        if (!inputValidation!!.isInputEditTextFilled(etfive, "Enter details")) {
                            return@setOnClickListener
                        } else {
                            uploadData("2", s1, s2, gender, s3, s4)
                        }
                    }
                } else
                    if (status == "update") {
                        title = "Update missing person"
                        val model = intent.getSerializableExtra("MODEL") as Missings

                        etone.setText(model.name)
                        ettwo.setText(model.age)
                        etthree.setText(model.lastseen)
                        etfive.setText(model.details)
                        Picasso.get().load(model.thumbnail).placeholder(R.drawable.ic_add_a_photo).into(display_image)

                        submit.setOnClickListener {
                            s1 = etone.text.toString()      //name
                            s2 = ettwo.text.toString()      //age
                            s3 = etthree.text.toString()    //last seen
                            s4 = etfive.text.toString()     //details

                            if (!inputValidation!!.isInputEditTextFilled(etone, "Enter name")) {
                                return@setOnClickListener
                            }
                            if (!inputValidation!!.isInputEditTextFilled(ettwo, "Enter age")) {
                                return@setOnClickListener
                            }
                            if (!inputValidation!!.isInputEditTextFilled(etthree, "Enter last seen")) {
                                return@setOnClickListener
                            }
                            if (!inputValidation!!.isInputEditTextFilled(etfive, "Enter details")) {
                                return@setOnClickListener
                            } else {
                                mProgress!!.setTitle("Updating...")
                                mProgress!!.setMessage("Please wait while we check your credentials.")
                                mProgress!!.setCanceledOnTouchOutside(false)
                                mProgress!!.show()
                                val ref = CR.getStorageRef().child("images").child(model.key.toString())
                                val time =
                                    SimpleDateFormat("EEE, d MMM yyyy")
                                        .format(Date(System.currentTimeMillis()))
                                var m: Any? = null
                                if (filepath != null) {
                                    val uploadTask = ref.putFile(filepath!!)
                                    val urlTask =
                                        uploadTask.continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>> { task ->
                                            if (!task.isSuccessful) {
                                                task.exception?.let {
                                                    throw it
                                                }
                                            }
                                            return@Continuation ref.downloadUrl
                                        }).addOnCompleteListener { task ->
                                            if (task.isSuccessful) {
                                                val downloadUri = task.result

                                                Log.e("IMAGE URL: ", downloadUri.toString())


                                                m = Missings(
                                                    model.key,
                                                    CR.getUserId(),
                                                    s1,
                                                    s2,
                                                    gender,
                                                    s3,
                                                    s4,
                                                    downloadUri.toString(),
                                                    model.status,
                                                    time
                                                )
                                                CR.getDatabaseRef().child(CR.ALLMISSINGS)!!
                                                    .child(model.key)
                                                    .setValue(m!!)
                                                    .addOnCompleteListener { task ->
                                                        if (task.isSuccessful) {
                                                            mProgress!!.dismiss()
                                                            finish()
                                                            toast("Data updated successfully!")
                                                        } else {
                                                            toast("Something went wrong")
                                                            mProgress!!.dismiss()
                                                        }
                                                    }
                                            } else {
                                                // Handle failures
                                                toast("Something went wrong!")
                                                mProgress!!.dismiss()
                                            }
                                        }
                                } else {
                                    m = Missings(
                                        model.key,
                                        CR.getUserId(),
                                        s1,
                                        s2,
                                        gender,
                                        s3,
                                        s4,
                                        model.thumbnail,
                                        model.status,
                                        time
                                    )
                                    CR.getDatabaseRef().child(CR.ALLMISSINGS)!!
                                        .child(model.key)
                                        .setValue(m!!)
                                        .addOnCompleteListener { task ->
                                            if (task.isSuccessful) {
                                                mProgress!!.dismiss()
                                                finish()
                                                toast("Data updated successfully!")
                                            } else {
                                                toast("Something went wrong")
                                                mProgress!!.dismiss()
                                            }
                                        }
                                }

                            }
                        }
                    }
            }
        }

        browse_image.setOnClickListener {
            Permissions.check(this/*context*/, permissions, rationale, options, object : PermissionHandler() {
                override fun onGranted() {
                    imageDialog()
                }

                override fun onDenied(context: Context?, deniedPermissions: ArrayList<String>?) {
                    toast("Permission required!")
                }

            })
        }
    }

    fun imageDialog() {
        val options = arrayOf<CharSequence>("Take Photo", "Choose from Gallery", "Cancel")

        val builder = AlertDialog.Builder(this)
        builder.setTitle("Add Photo!")

        builder.setItems(options) { dialog, item ->
            if (options[item] == "Take Photo") {

                val takePicture = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                startActivityForResult(takePicture, CAMERA)

            } else if (options[item] == "Choose from Gallery") {

                val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                startActivityForResult(intent, GALLERY)

            } else if (options[item] == "Cancel") {
                dialog.dismiss()
            }
        }

        builder.show()
    }

    //Get images from gallery and camera
    fun getImage(data: Intent, code: Int) {
        val contentURI: Uri?
        val bundle: Bundle?

        var bit: Bitmap? = null
        if (code == GALLERY) {
            contentURI = data.data
            filepath = data.data
            try {
                bit = MediaStore.Images.Media.getBitmap(this.contentResolver, contentURI)
            } catch (e: IOException) {
                e.printStackTrace()
            }

        } else if (code == CAMERA) {
            bundle = data.extras
            if (bundle!!.get("data") != null)
                bit = bundle.get("data") as Bitmap
        }
        val byteArrayOutputStream = ByteArrayOutputStream()
        bit!!.compress(Bitmap.CompressFormat.PNG, 5, byteArrayOutputStream)
        display_image.setImageBitmap(bit)
        filepath = Uri.parse(MediaStore.Images.Media.insertImage(contentResolver, bit, null, null))

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK && requestCode == GALLERY) run {
            if (data != null) {
                getImage(data, requestCode)
            }
        } else if (resultCode == Activity.RESULT_OK && requestCode == CAMERA) {
            if (data != null) {
                getImage(data, requestCode)
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            android.R.id.home -> onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    fun uploadData(status: String, one: String, two: String, three: String, four: String, five: String) {
        mProgress!!.setTitle("Adding...")
        mProgress!!.setMessage("Please wait while we check your credentials.")
        mProgress!!.setCanceledOnTouchOutside(false)
        mProgress!!.show()

        var key: String? = null
        var reference: DatabaseReference? = null
        var model: Any? = null

        when (status) {
            // 0: crime     1: complaint        2:missing
            "0" -> {
                reference = CR.getDatabaseRef().child(CR.ALLCRIMERS)
                key = CR.getDatabaseRef().child(CR.ALLCRIMERS).push().key
            }
            "1" -> {
                reference = CR.getDatabaseRef().child(CR.ALLCOMPLAINTS)
                key = CR.getDatabaseRef().child(CR.ALLCOMPLAINTS).push().key
            }
            "2" -> {
                reference = CR.getDatabaseRef().child(CR.ALLMISSINGS)
                key = CR.getDatabaseRef().child(CR.ALLMISSINGS).push().key
            }
        }

        val ref = CR.getStorageRef().child("images").child(key!!)
        val uploadTask = ref.putFile(filepath!!)

        val urlTask = uploadTask.continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>> { task ->
            if (!task.isSuccessful) {
                task.exception?.let {
                    throw it
                }
            }
            return@Continuation ref.downloadUrl
        }).addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val downloadUri = task.result

                Log.e("IMAGE URL: ", downloadUri.toString())

                val time =
                    SimpleDateFormat("EEE, d MMM yyyy")
                        .format(Date(System.currentTimeMillis()))

                when (status) {
                    // 0: crime     1: complaint        2:missing
                    "0" -> {
                        model = Crimes(
                            key,
                            CR.getUserId(),
                            one,
                            two,
                            three,
                            four,
                            downloadUri.toString(),
                            "submitted",
                            time
                        )
                    }
                    "1" -> {
                        model = Complaints(
                            key,
                            CR.getUserId(),
                            one,
                            two,
                            three,
                            four,
                            five,
                            downloadUri.toString(),
                            "submitted",
                            time
                        )
                    }
                    "2" -> {
                        model = Missings(
                            key,
                            CR.getUserId(),
                            one,
                            two,
                            three,
                            four,
                            five,
                            downloadUri.toString(),
                            "submitted",
                            time
                        )
                    }
                }
                reference!!
                    .child(key)
                    .setValue(model!!)
                    .addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                            mProgress!!.dismiss()
                            finish()
                            toast("Data added successfully!")
                        } else {
                            toast("Something went wrong")
                            mProgress!!.dismiss()
                        }
                    }

            } else {
                // Handle failures
                toast("Something went wrong!")
                mProgress!!.dismiss()
            }
        }
    }
}
