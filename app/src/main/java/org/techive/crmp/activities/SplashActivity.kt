package org.techive.crmp.activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.WindowManager
import android.widget.Toast
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import org.jetbrains.anko.*
import org.techive.crmp.R
import org.techive.crmp.helper.CR
import org.techive.crmp.models.User

class SplashActivity : AppCompatActivity() {

    private val SPLASH_TIME_OUT = 400

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )

        Handler().postDelayed({
            if (CR.isSignedIn()) {
                Log.e("isSignIn", "User is signed in " + CR.getUserId())
                CR.getDatabaseRef()
                    .child("users")
                    .child(CR.getUserId())
                    .addListenerForSingleValueEvent(object : ValueEventListener {
                        override fun onDataChange(dataSnapshot: DataSnapshot) {
                            Log.e("OK", "OK")
                            val u = dataSnapshot.getValue<User>(User::class.java)
                            if (u != null) {
                                CR.u = u
                                startActivity(Intent(this@SplashActivity, MainActivity::class.java))
                                finish()
                            } else {
                                toast(resources.getString(R.string.something_went_wrong))
                            }
                        }

                        override fun onCancelled(databaseError: DatabaseError) {
                            Log.e("Error", databaseError.toException().message)
                        }
                    })
            } else {
                startActivity(Intent(this@SplashActivity, HomeActivity::class.java))
                finish()
            }
        }, SPLASH_TIME_OUT.toLong())

    }
}
