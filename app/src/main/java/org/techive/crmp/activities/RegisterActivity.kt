package org.techive.crmp.activities

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.MenuItem
import android.view.WindowManager
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_register.*
import org.jetbrains.anko.toast
import org.techive.crmp.R
import org.techive.crmp.helper.CR
import org.techive.crmp.helper.InputValidation
import org.techive.crmp.models.User
import java.text.SimpleDateFormat
import java.util.*

class RegisterActivity : AppCompatActivity() {

    var inputValidation: InputValidation? = null
    private var mProgress: ProgressDialog? = null

    internal var n: String = ""
    internal var e: String = ""
    internal var p: String = ""
    internal var ph: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)
        inputValidation = InputValidation(this)
        mProgress = ProgressDialog(this)

        title = "Register"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        register.setOnClickListener {
            n = name.text.toString()
            e = email.text.toString()
            ph = phone.text.toString()
            p = password.text.toString()

            if (!inputValidation!!.isInputEditTextFilled(name, getString(R.string.error_message_name))) {
                return@setOnClickListener
            }
            if (!inputValidation!!.isInputEditTextFilled(email, getString(R.string.error_message_email))) {
                return@setOnClickListener
            }
            if (!inputValidation!!.isInputEditTextEmail(email, getString(R.string.error_message_email))) {
                return@setOnClickListener
            }
            if (!inputValidation!!.isInputEditTextFilled(phone, getString(R.string.error_message_phone))) {
                return@setOnClickListener
            }
            if (!inputValidation!!.isInputEditTextFilled(password, getString(R.string.error_message_password))) {
                return@setOnClickListener
            } else {
                mProgress!!.setTitle("Signing up")
                mProgress!!.setMessage("Please wait while we check your credentials.")
                mProgress!!.setCanceledOnTouchOutside(false)
                mProgress!!.show()

                CR.getAuth().createUserWithEmailAndPassword(e, p)
                    .addOnCompleteListener(this) { task ->
                        if (task.isSuccessful) {
                            Log.d("Login", "signInWithEmail:success")

                            val time =
                                SimpleDateFormat("EEE, d MMM yyyy")
                                    .format(Date(System.currentTimeMillis()))

                            val user = User(
                                CR.getUserId(),
                                n,
                                e,
                                ph,
                                p,
                                "user",
                                time
                            )
                            CR.getDatabaseRef().child("users")
                                .child(CR.getUserId()).setValue(user)
                            CR.u = user
                            mProgress!!.dismiss()
                            val intent = Intent(this@RegisterActivity, MainActivity::class.java)
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                            startActivity(intent)

                        } else {
                            mProgress!!.dismiss()
                            // If sign in fails, display a message to the user.
                            Log.w("Login", "signInWithEmail:failure", task.exception)
                            toast(task.exception!!.message.toString())
                        }
                    }
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            android.R.id.home -> {

                onBackPressed()

            }
        }
        return super.onOptionsItemSelected(item)
    }
}
