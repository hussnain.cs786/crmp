package org.techive.crmp.activities

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.design.widget.TabLayout
import android.support.v7.app.AppCompatActivity;
import android.view.Menu
import android.view.MenuItem
import android.view.View

import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import org.jetbrains.anko.toast
import org.techive.crmp.R
import org.techive.crmp.adapters.PagerAdapter
import org.techive.crmp.helper.CR

class MainActivity : AppCompatActivity() {

    var fragStatus = 0

    @SuppressLint("RestrictedApi")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportActionBar!!.elevation = 0.0f
        configureTabLayout()

        if (CR.u!!.userType == "admin"){
            fab.visibility = View.GONE
        }
        fab.setOnClickListener { view ->
            val intent = Intent(this@MainActivity, AddActivity::class.java)
            intent.putExtra(CR.FRAGSTATUS,fragStatus.toString())
            intent.putExtra(CR.DATASTATUS,"add")
            startActivity(intent)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_logout -> {
                CR.getAuth().signOut()
                startActivity(Intent(this@MainActivity, HomeActivity::class.java))
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun configureTabLayout() {

        tab_layout!!.addTab(tab_layout!!.newTab().setText("All Crimes"))
        tab_layout!!.addTab(tab_layout!!.newTab().setText("All Complaints"))
        tab_layout!!.addTab(tab_layout!!.newTab().setText("Missing Persons"))

        val adapter = PagerAdapter(super.getSupportFragmentManager(), tab_layout!!.tabCount)
        pager!!.adapter = adapter

        pager!!.addOnPageChangeListener(
            TabLayout.TabLayoutOnPageChangeListener(tab_layout))
        tab_layout!!.addOnTabSelectedListener(object :
            TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                pager!!.currentItem = tab.position
                when (tab.position) {
                    0 -> {
                        fragStatus = 0
                    }
                    1 -> {
                        fragStatus = 1
                    }
                    2 -> {
                        fragStatus = 2
                    }
                }
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {
                when (tab.position) {
                    0 -> {
//                        tab.setIcon(R.drawable.courses_stroke)
                    }
                    1 -> {
//                        tab.setIcon(R.drawable.services_stroke)
                    }
                    2 -> {
//                        tab.setIcon(R.drawable.portfolio_stroke)
                    }
                }
            }

            override fun onTabReselected(tab: TabLayout.Tab) {

            }

        })
    }
}
