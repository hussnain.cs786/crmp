package org.techive.crmp.activities

import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_detail.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.spinner_dialog.*
import org.jetbrains.anko.toast
import org.techive.crmp.R
import org.techive.crmp.helper.CR
import org.techive.crmp.models.Complaints
import org.techive.crmp.models.Crimes
import org.techive.crmp.models.Missings

class DetailActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener {
    override fun onNothingSelected(p0: AdapterView<*>?) {

    }

    override fun onItemSelected(
        adapterView: AdapterView<*>?,
        p1: View?,
        position: Int,
        p3: Long
    ) {
        s = adapterView!!.getItemAtPosition(position).toString()
    }

    val filter = arrayOf("Submitted","Seen","Processing","Completed")
    lateinit var arrayAdapter: ArrayAdapter<*>
    var s: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        //crime: street, city, zipcode, details, image
        //complaints: address, city, pincode, subject, complaints, photo
        //missing: name, age, gender, last seen, details, image

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        // 0: crime     1: complaint        2:missing
        val status = intent.getStringExtra(CR.DETAILSTATUS)

        if (CR.u!!.userType == "admin"){
            edit_status.visibility = View.VISIBLE
        }else{
            edit_status.visibility = View.GONE
        }

        when (status) {
            "0" -> {
                //crime: street, city, zipcode, details, image
                title = "Crime Details"
                val model = intent.getSerializableExtra("M") as Crimes

                t1.text = "CITY"
                t2.text = model.city
                t3.text = "STREET"
                t4.text = model.street
                t5.text = "ZIPCODE"
                t6.text = model.zipcode
                t7.text = "DETAIL"
                t8.text = model.details
                t9.text = "STATUS"
                t10.text = model.status
                t10.setTextColor(resources.getColor(R.color.colorRed))

                t11.visibility = View.GONE
                t12.visibility = View.GONE

                v2.visibility = View.GONE
                v3.visibility = View.GONE

                Picasso.get().load(model.thumbnail).placeholder(R.drawable.placeholder).into(thumbnail)

                edit_status.setOnClickListener {
                    //Inflate the dialog with custom view
                    val mDialogView = LayoutInflater.from(this).inflate(R.layout.spinner_dialog, null)
                    //AlertDialogBuilder
                    val mBuilder = AlertDialog.Builder(this)
                        .setView(mDialogView)
                        .setTitle("Edit Status")

                    //show dialog
                    val mAlertDialog = mBuilder.show()

                    arrayAdapter = ArrayAdapter(this, R.layout.spinner_list, filter)
                    arrayAdapter.setDropDownViewResource(R.layout.spinner_item)

                    mAlertDialog.status_spinner.adapter = arrayAdapter
                    mAlertDialog.status_spinner.onItemSelectedListener = this

                    mAlertDialog.update_status.setOnClickListener {
                        CR.getDatabaseRef().child(CR.ALLCRIMERS)
                            .child(model.key)
                            .child("status")
                            .setValue(s.toString())
                            .addOnCompleteListener { task ->
                                if (task.isSuccessful) {
                                    t12.text = s.toString()
                                    toast("status updated!")
                                    mAlertDialog.dismiss()
                                }
                            }
                    }
                }

            }
            "1" -> {
                //complaints: address, city, pincode, subject, complaints, photo
                title = "Complaint Details"
                val model = intent.getSerializableExtra("M") as Complaints

                t1.text = "SUBJECT"
                t2.text = model.subject
                t3.text = "ADDRESS"
                t4.text = model.address
                t5.text = "CITY"
                t6.text = model.city
                t7.text = "PINCODE"
                t8.text = model.pincode
                t9.text = "DETAIL"
                t10.text = model.complaint
                t11.text = "STATUS"
                t12.text = model.status
                t12.setTextColor(resources.getColor(R.color.colorRed))

                v3.visibility = View.GONE

                Picasso.get().load(model.thumbnail).placeholder(R.drawable.placeholder).into(thumbnail)

                edit_status.setOnClickListener {
                    //Inflate the dialog with custom view
                    val mDialogView = LayoutInflater.from(this).inflate(R.layout.spinner_dialog, null)
                    //AlertDialogBuilder
                    val mBuilder = AlertDialog.Builder(this)
                        .setView(mDialogView)
                        .setTitle("Edit Status")

                    //show dialog
                    val mAlertDialog = mBuilder.show()

                    arrayAdapter = ArrayAdapter(this, R.layout.spinner_list, filter)
                    arrayAdapter.setDropDownViewResource(R.layout.spinner_item)

                    mAlertDialog.status_spinner.adapter = arrayAdapter
                    mAlertDialog.status_spinner.onItemSelectedListener = this

                    mAlertDialog.update_status.setOnClickListener {
                        CR.getDatabaseRef().child(CR.ALLCOMPLAINTS)
                            .child(model.key)
                            .child("status")
                            .setValue(s.toString())
                            .addOnCompleteListener { task ->
                                if (task.isSuccessful) {
                                    t12.text = s.toString()
                                    toast("status updated!")
                                    mAlertDialog.dismiss()
                                }
                            }
                    }
                }
            }
            "2" -> {
                //missing: name, age, gender, last seen, details, image
                title = "Missing Person Details"
                val model = intent.getSerializableExtra("M") as Missings

                t1.text = "NAME"
                t2.text = model.name
                t3.text = "GENDER"
                t4.text = model.gender
                t5.text = "AGE"
                t6.text = model.age
                t7.text = "LAST SEEN"
                t8.text = model.lastseen
                t9.text = "DETAIL"
                t10.text = model.details
                t11.text = "STATUS"
                t12.text = model.status
                t12.setTextColor(resources.getColor(R.color.colorRed))

                v3.visibility = View.GONE

                Picasso.get().load(model.thumbnail).placeholder(R.drawable.placeholder).into(thumbnail)

                edit_status.setOnClickListener {
                    //Inflate the dialog with custom view
                    val mDialogView = LayoutInflater.from(this).inflate(R.layout.spinner_dialog, null)
                    //AlertDialogBuilder
                    val mBuilder = AlertDialog.Builder(this)
                        .setView(mDialogView)
                        .setTitle("Edit Status")

                    //show dialog
                    val mAlertDialog = mBuilder.show()

                    arrayAdapter = ArrayAdapter(this, R.layout.spinner_list, filter)
                    arrayAdapter.setDropDownViewResource(R.layout.spinner_item)

                    mAlertDialog.status_spinner.adapter = arrayAdapter
                    mAlertDialog.status_spinner.onItemSelectedListener = this

                    mAlertDialog.update_status.setOnClickListener {
                        CR.getDatabaseRef().child(CR.ALLMISSINGS)
                            .child(model.key)
                            .child("status")
                            .setValue(s.toString())
                            .addOnCompleteListener { task ->
                                if (task.isSuccessful) {
                                    t12.text = s.toString()
                                    toast("status updated!")
                                    mAlertDialog.dismiss()
                                }
                            }
                    }
                }
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            android.R.id.home -> onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }
}
