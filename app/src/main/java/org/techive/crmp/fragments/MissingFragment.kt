package org.techive.crmp.fragments

import android.app.ProgressDialog
import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.fragment_complaints_list.progressBar
import kotlinx.android.synthetic.main.fragment_missing_list.*

import org.techive.crmp.R
import org.techive.crmp.adapters.RecyclerViewAdapter
import org.techive.crmp.helper.CR
import org.techive.crmp.models.Missings

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class MissingFragment : Fragment() {
    private var param1: String? = null
    private var param2: String? = null
    private var listener: OnFragmentInteractionListener? = null

    var mList: MutableList<Any> = ArrayList()
    var mAdapter: RecyclerViewAdapter? = null
    private var mListType = RecyclerViewAdapter.VIEW_TYPE_MISSING
    private var mProgress: ProgressDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_missing_list, container, false)

        CR.getDatabaseRef().child(CR.ALLMISSINGS).addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError?) {

            }

            override fun onDataChange(dataSnapshot: DataSnapshot?) {
                Log.e("WORKERS LIST:", "OK")
                mList.clear()
                for (dataSnapshot1 in dataSnapshot!!.children) {

                    val model = dataSnapshot1.getValue(Missings::class.java)!!
                    if(CR.u!!.userType=="user") {
                        if (model.uid!!.toLowerCase() == CR.getUserId().toLowerCase())
                            mList.add(0, model)
                    }else{
                        mList.add(0, model)
                    }
                    progressBar.visibility = View.GONE
                }
                mAdapter = RecyclerViewAdapter(mList,  mListType)
                with(missing_list) {
                    layoutManager = android.support.v7.widget.LinearLayoutManager(context)

                    adapter = mAdapter
                    setEmptyView(empty_missing_view)
                    progressBar.visibility = android.view.View.GONE
                }
            }

        })

        return view
    }

    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        }
//        else {
//            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
//        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnFragmentInteractionListener {
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            MissingFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}
