package org.techive.crmp.adapters

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.cardview_layout.view.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.cancelButton
import org.jetbrains.anko.toast
import org.techive.crmp.R
import org.techive.crmp.activities.AddActivity
import org.techive.crmp.activities.DetailActivity
import org.techive.crmp.helper.CR
import org.techive.crmp.models.Complaints
import org.techive.crmp.models.Crimes
import org.techive.crmp.models.Missings

class RecyclerViewAdapter(
    private var mValues: MutableList<Any>,
    private val mViewType: Int
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val mOnClickListener: View.OnClickListener = View.OnClickListener { v ->
        //            val item = v.tag as DummyItem
//            // Notify the active callbacks interface (the activity, if the fragment is attached to
//            // one) that an item has been selected.
//            mListener?.onListFragmentInteraction(item)
    }

    private var listener: OnItemClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)

        return when (mViewType) {
            VIEW_TYPE_CRIME -> CrimeViewHolder(inflater.inflate(R.layout.cardview_layout, parent, false))
            VIEW_TYPE_COMPLAINTS -> ComplaintsViewHolder(inflater.inflate(R.layout.cardview_layout, parent, false))
            else -> MissingViewHolder(inflater.inflate(R.layout.cardview_layout, parent, false))
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = mValues[position]

        when (mViewType) {

            VIEW_TYPE_CRIME -> {
                val m = item as Crimes
                val h = holder as CrimeViewHolder

                h.tvOne.text = m.city

                h.tvTwo.text = "Zpicode:"
                h.tvThree.text = m.zipcode

                h.tvFour.text = "Date:"
                h.tvFive.text = m.createdAt

                h.tvSix.text = "Status:"
                h.tvSeven.text = m.status

                Picasso.get().load(m.thumbnail).placeholder(R.drawable.placeholder).into(h.image)
                h.mView.setOnClickListener {
                    val intent = Intent(h.itemView.context, DetailActivity::class.java)
                    intent.putExtra(CR.DETAILSTATUS, "0")
                    intent.putExtra("M", m)
                    h.itemView.context.startActivity(intent)
                }

                h.itemView.setOnLongClickListener {
                    if (CR.isSignedIn()) {
                        h.mView.context.alert {
                            title = "Update or Delete?"
                            positiveButton("Delete") {
                                h.itemView.context.alert {
                                    title = "Are you sure?"
                                    positiveButton("Yes") {
                                        CR.getDatabaseRef().child(CR.ALLCRIMERS)
                                            .child(m.key)
                                            .removeValue()
                                            .addOnCompleteListener { task ->
                                                if (task.isSuccessful) {
                                                    h.mView.context.toast("Deleted!")
                                                }
                                            }
                                    }
                                    cancelButton { }
                                }.show()
                            }
                            negativeButton("Edit") {
                                val intent = Intent(h.itemView.context, AddActivity::class.java)
                                intent.putExtra(CR.FRAGSTATUS, "0")
                                intent.putExtra(CR.DATASTATUS, "update")
                                intent.putExtra("MODEL", m)
                                h.itemView.context.startActivity(intent)
                            }
                            neutralPressed("Cancel") {

                            }
                        }.show()
                    }
                    true
                }
            }

            VIEW_TYPE_COMPLAINTS -> {
                val m = item as Complaints
                val h = holder as ComplaintsViewHolder

                h.tvOne.text = m.city

                h.tvTwo.text = "Subject:"
                h.tvThree.text = m.subject

                h.tvFour.text = "Date:"
                h.tvFive.text = m.createdAt

                h.tvSix.text = "Status:"
                h.tvSeven.text = m.status

                Picasso.get().load(m.thumbnail).placeholder(R.drawable.placeholder).into(h.image)
                h.mView.setOnClickListener {
                    val intent = Intent(h.itemView.context, DetailActivity::class.java)
                    intent.putExtra(CR.DETAILSTATUS, "1")
                    intent.putExtra("M", m)
                    h.itemView.context.startActivity(intent)
                }

                h.itemView.setOnLongClickListener {
                    if (CR.isSignedIn()) {
                        h.mView.context.alert {
                            title = "Update or Delete?"
                            positiveButton("Delete") {
                                h.itemView.context.alert {
                                    title = "Are you sure?"
                                    positiveButton("Yes") {
                                        CR.getDatabaseRef().child(CR.ALLCOMPLAINTS)
                                            .child(m.key)
                                            .removeValue()
                                            .addOnCompleteListener { task ->
                                                if (task.isSuccessful) {
                                                    h.mView.context.toast("Deleted!")
                                                }
                                            }
                                    }
                                    cancelButton { }
                                }.show()
                            }
                            negativeButton("Edit") {
                                val intent = Intent(h.itemView.context, AddActivity::class.java)
                                intent.putExtra(CR.FRAGSTATUS, "1")
                                intent.putExtra(CR.DATASTATUS, "update")
                                intent.putExtra("MODEL", m)
                                h.itemView.context.startActivity(intent)
                            }
                            neutralPressed("Cancel") {

                            }
                        }.show()
                    }
                    true
                }
            }

            else -> {
                val m = item as Missings
                val h = holder as MissingViewHolder
                h.tvOne.text = m.name

                h.tvTwo.text = "Age:"
                h.tvThree.text = m.age

                h.tvFour.text = "Date:"
                h.tvFive.text = m.createdAt

                h.tvSix.text = "Status:"
                h.tvSeven.text = m.status

                Picasso.get().load(m.thumbnail).placeholder(R.drawable.placeholder).into(h.image)

                h.mView.setOnClickListener {
                    val intent = Intent(h.itemView.context, DetailActivity::class.java)
                    intent.putExtra(CR.DETAILSTATUS, "2")
                    intent.putExtra("M", m)
                    h.itemView.context.startActivity(intent)
                }

                h.itemView.setOnLongClickListener {
                    if (CR.isSignedIn()) {
                        h.mView.context.alert {
                            title = "Update or Delete?"
                            positiveButton("Delete") {
                                h.itemView.context.alert {
                                    title = "Are you sure?"
                                    positiveButton("Yes") {
                                        CR.getDatabaseRef().child(CR.ALLMISSINGS)
                                            .child(m.key)
                                            .removeValue()
                                            .addOnCompleteListener { task ->
                                                if (task.isSuccessful) {
                                                    h.mView.context.toast("Deleted!")
                                                }
                                            }
                                    }
                                    cancelButton { }
                                }.show()
                            }
                            negativeButton("Edit") {
                                val intent = Intent(h.itemView.context, AddActivity::class.java)
                                intent.putExtra(CR.FRAGSTATUS, "2")
                                intent.putExtra(CR.DATASTATUS, "update")
                                intent.putExtra("MODEL", m)
                                h.itemView.context.startActivity(intent)
                            }
                            neutralPressed("Cancel") {

                            }
                        }.show()
                    }
                    true
                }
            }
        }

    }

    override fun getItemCount(): Int = mValues.size

    inner class CrimeViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val tvOne = mView.tvone
        val tvTwo = mView.tvtwo
        val tvThree = mView.tvthree
        val tvFour = mView.tvfour
        val tvFive = mView.tvfive
        val tvSix = mView.tvsix
        val tvSeven = mView.tvseven
        val image = mView.image
    }

    inner class ComplaintsViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val tvOne = mView.tvone
        val tvTwo = mView.tvtwo
        val tvThree = mView.tvthree
        val tvFour = mView.tvfour
        val tvFive = mView.tvfive
        val tvSix = mView.tvsix
        val tvSeven = mView.tvseven
        val image = mView.image
    }

    inner class MissingViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val tvOne = mView.tvone
        val tvTwo = mView.tvtwo
        val tvThree = mView.tvthree
        val tvFour = mView.tvfour
        val tvFive = mView.tvfive
        val tvSix = mView.tvsix
        val tvSeven = mView.tvseven
        val image = mView.image
    }

    companion object {
        val VIEW_TYPE_CRIME = 0
        val VIEW_TYPE_COMPLAINTS = 1
        val VIEW_TYPE_MISSING = 2
    }


    //load fragment on recyclerView OnClickListner
    fun setOnItemClickListener(listener: OnItemClickListener) {
        this.listener = listener
    }

    interface OnItemClickListener {

        fun onItemClick(model: Any, position: Int, s: String)
    }

    fun setFilter(newList: ArrayList<Any>) {
        mValues = ArrayList()
        (mValues as ArrayList<Any>).addAll(newList)
        notifyDataSetChanged()
    }

}