package org.techive.crmp.adapters

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import org.techive.crmp.fragments.ComplaintsFragment
import org.techive.crmp.fragments.CrimeFragment
import org.techive.crmp.fragments.MissingFragment

class PagerAdapter(fm: FragmentManager, private var tabCount: Int) :
        FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment? {

        return when (position) {
            0 -> CrimeFragment()
            1 -> ComplaintsFragment()
            2 -> MissingFragment()
            else -> null
        }
    }

    override fun getCount(): Int {
        return tabCount
    }
}