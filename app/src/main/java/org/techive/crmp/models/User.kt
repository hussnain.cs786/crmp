package org.techive.crmp.models

import java.io.Serializable

class User : Serializable {

    var uid: String? = null
    var name: String? = null
    var email: String? = null
    var phone: String? = null
    var password: String? = null
    var userType: String? = null
    var createdAt: String? = null

    constructor() {}
    constructor(
        uid: String?,
        name: String?,
        email: String?,
        phone: String?,
        password: String?,
        userType: String?,
        createdAt: String?
    ) {
        this.uid = uid
        this.name = name
        this.email = email
        this.phone = phone
        this.password = password
        this.userType = userType
        this.createdAt = createdAt
    }


}