package org.techive.crmp.models

import java.io.Serializable

class Crimes : Serializable {
    //crime: street, city, zipcode, details, image
    var key: String? = null
    var uid: String? = null
    var street: String? = null
    var city: String? = null
    var zipcode: String? = null
    var details: String? = null
    var thumbnail: String? = null
    var status: String? = null
    var createdAt: String? = null

    constructor() {}
    constructor(
        key: String?,
        uid: String?,
        street: String?,
        city: String?,
        zipcode: String?,
        details: String?,
        thumbnail: String?,
        status: String?,
        createdAt: String?
    ) {
        this.key = key
        this.uid = uid
        this.street = street
        this.city = city
        this.zipcode = zipcode
        this.details = details
        this.thumbnail = thumbnail
        this.status = status
        this.createdAt = createdAt
    }


}