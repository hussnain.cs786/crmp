package org.techive.crmp.models

import java.io.Serializable

class Complaints : Serializable {
    //complaints: address, city, pincode, subject, complaints, photo
    var key: String? = null
    var uid: String? = null
    var address: String? = null
    var city: String? = null
    var pincode: String? = null
    var subject: String? = null
    var complaint: String? = null
    var thumbnail: String? = null
    var status: String? = null
    var createdAt: String? = null

    constructor() {}
    constructor(
        key: String?,
        uid: String?,
        address: String?,
        city: String?,
        pincode: String?,
        subject: String?,
        complaint: String?,
        thumbnail: String?,
        status: String?,
        createdAt: String?
    ) {
        this.key = key
        this.uid = uid
        this.address = address
        this.city = city
        this.pincode = pincode
        this.subject = subject
        this.complaint = complaint
        this.thumbnail = thumbnail
        this.status = status
        this.createdAt = createdAt
    }


}