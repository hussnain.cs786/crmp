package org.techive.crmp.models

import java.io.Serializable

class Missings : Serializable {
    //missing: name, age, gender, last seen, details, image
    var key: String? = null
    var uid: String? = null
    var name: String? = null
    var age: String? = null
    var gender: String? = null
    var lastseen: String? = null
    var details: String? = null
    var thumbnail: String? = null
    var status: String? = null
    var createdAt: String? = null

    constructor() {}
    constructor(
        key: String?,
        uid: String?,
        name: String?,
        age: String?,
        gender: String?,
        lastseen: String?,
        details: String?,
        thumbnail: String?,
        status: String?,
        createdAt: String?
    ) {
        this.key = key
        this.uid = uid
        this.name = name
        this.age = age
        this.gender = gender
        this.lastseen = lastseen
        this.details = details
        this.thumbnail = thumbnail
        this.status = status
        this.createdAt = createdAt
    }


}